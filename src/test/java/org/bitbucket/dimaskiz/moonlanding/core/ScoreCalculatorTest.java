package org.bitbucket.dimaskiz.moonlanding.core;

import org.bitbucket.dimaskiz.moonlanding.config.Config;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ScoreCalculatorTest {

    private Config config;

    @BeforeMethod
    public void beforeMethod() {
        config = new Config();
    }

    @Test
    public void fuelScoreTest() {
        Telemetry telemetry = new Telemetry(config);
        telemetry.setFuel(0);
        Assert.assertEquals(ScoreCalculator.fuelScore(config, telemetry), 100);

        telemetry.setFuel(config.getInitialFuel());
        Assert.assertEquals(ScoreCalculator.fuelScore(config, telemetry), 400);
    }

    @Test
    public void speedScoreTest() {
        Telemetry telemetry = new Telemetry(config);
        telemetry.setSpeed(config.getMaxSpeed());
        Assert.assertEquals(ScoreCalculator.speedScore(config, telemetry), 0);

        telemetry.setSpeed(0);
        Assert.assertEquals(ScoreCalculator.speedScore(config, telemetry), 300);
    }

    @Test
    public void timeScoreTest() {
        Telemetry telemetry = new Telemetry(config);
        double freeFallTimeSeconds = Math.sqrt(2 * config.getInitialAltitude() / config.getMoonGravity());
        telemetry.setRunTime(Math.round(freeFallTimeSeconds * 1000));
        Assert.assertEquals(ScoreCalculator.timeScore(config, telemetry), 300);

        telemetry.setRunTime(Math.round(freeFallTimeSeconds * 1000) * 10000);
        Assert.assertEquals(ScoreCalculator.timeScore(config, telemetry), 0);
    }
}