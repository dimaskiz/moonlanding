package org.bitbucket.dimaskiz.moonlanding.persistence;

import static org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao.MAX_STATS_COUNT;
import static org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao.NAME_MAX_LENGTH;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class StatsDaoTest {
    private StatsDao dao;

    @BeforeMethod
    public void beforeMethod() throws SQLException {
        Connection connection = new H2DBConnectionHandler("jdbc:h2:mem:").getConnection();
        Assert.assertFalse(connection.isClosed());
        dao = new StatsDao(connection);
        dao.init();
    }

    @Test
    public void insertAndListTest() {
        List<StatEntity> list = dao.list();
        Assert.assertTrue(list.isEmpty());

        dao.insert(StatEntity.builder().name("qqq").score(10).build());
        dao.insert(StatEntity.builder().name("eee").score(12).build());
        dao.insert(StatEntity.builder().name("aaa").score(3).build());

        List<StatEntity> list1 = dao.list();
        Assert.assertEquals(list1.size(), 3);
        Assert.assertTrue(list1.get(0).getStatId() > 0);
        Assert.assertEquals(list1.get(0).getName(), "eee");
        Assert.assertEquals(list1.get(0).getScore(), 12);

        Assert.assertTrue(list1.get(1).getStatId() > 0);
        Assert.assertEquals(list1.get(1).getName(), "qqq");
        Assert.assertEquals(list1.get(1).getScore(), 10);

        Assert.assertTrue(list1.get(2).getStatId() > 0);
        Assert.assertEquals(list1.get(2).getName(), "aaa");
        Assert.assertEquals(list1.get(2).getScore(), 3);
    }

    @Test
    public void listMaxLengthTest() {
        List<StatEntity> list = dao.list();
        Assert.assertTrue(list.isEmpty());

        for (int i = 0; i < 15; i++) {
            dao.insert(StatEntity.builder().name("a" + i).score(i).build());
        }

        List<StatEntity> list1 = dao.list();
        Assert.assertEquals(list1.size(), MAX_STATS_COUNT);
    }

    @Test
    public void insertNameValidationTest() {
        List<StatEntity> list = dao.list();
        Assert.assertTrue(list.isEmpty());

        dao.insert(StatEntity.builder().name("").score(1).build());
        Assert.assertTrue(dao.list().isEmpty());

        String maxLengthName = String.format("%0" + NAME_MAX_LENGTH + "d", 1);

        dao.insert(StatEntity.builder().name(maxLengthName + "1").score(1).build());
        Assert.assertTrue(dao.list().isEmpty());

        dao.insert(StatEntity.builder().name(maxLengthName).score(1).build());
        Assert.assertEquals(dao.list().size(), 1);
    }
}