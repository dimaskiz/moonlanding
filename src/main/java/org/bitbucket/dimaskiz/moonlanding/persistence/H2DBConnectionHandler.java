package org.bitbucket.dimaskiz.moonlanding.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class H2DBConnectionHandler {

    private Connection connection;

    public H2DBConnectionHandler(String dbUrl) {
        try {
            Connection con = DriverManager.getConnection(dbUrl);
            try (Statement st = con.createStatement()) {
                try (ResultSet rs = st.executeQuery("select 2 + 2;")) {
                    if (rs.next()) {
                        int result = rs.getInt(1);
                        if (result != 4) {
                            log.error("DB check result should be 4! Actual value is {}", result);
                        }
                    }
                }
            }
            connection = con;
        } catch (SQLException ex) {
            log.error("Can't create DB connection!", ex);
        }
    }
}
