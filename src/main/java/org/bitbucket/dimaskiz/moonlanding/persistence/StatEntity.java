package org.bitbucket.dimaskiz.moonlanding.persistence;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
@EqualsAndHashCode
public class StatEntity {

    private int statId;
    private String name;
    private long score;
}
