package org.bitbucket.dimaskiz.moonlanding.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StatsDao {
    public static final int NAME_MAX_LENGTH = 10;
    public static final int MAX_STATS_COUNT = 10;
    private static final String INIT_SQL = "create table if not exists stats(id int auto_increment, name varchar(10), score bigint);";
    private static final String SELECT_TOP_10_SQL = "select * from stats order by score desc, id desc limit " + MAX_STATS_COUNT +";";
    private static final String INSERT_SQL = "insert into stats values (default, ?, ?);";

    private final Connection connection;

    public StatsDao(Connection connection) {
        this.connection = connection;
    }

    public void init() {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            log.error("Can't init stats", e);
            return;
        }
        try (Statement st = connection.createStatement()) {
            st.executeUpdate(INIT_SQL);
        } catch (SQLException e) {
            log.error("Can't init stats", e);
        }
    }

    public List<StatEntity> list() {
        List<StatEntity> result = new ArrayList<>();
        try (Statement st = connection.createStatement();
             ResultSet resultSet = st.executeQuery(SELECT_TOP_10_SQL)) {
            while (resultSet.next()) {
                result.add(StatEntity.builder()
                        .statId(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .score(resultSet.getLong("score"))
                        .build());
            }
        } catch (SQLException e) {
            log.error("Can't list stats", e);
        }
        return result;
    }

    public void insert(StatEntity statEntity) {
        assert statEntity.getStatId() == 0;
        assert statEntity.getName() != null;
        if (statEntity.getName() == null || statEntity.getName().trim().length() > NAME_MAX_LENGTH || statEntity.getName().trim().length() == 0) {
            return;
        }
        try (PreparedStatement st = connection.prepareStatement(INSERT_SQL)) {
            st.setString(1, statEntity.getName());
            st.setLong(2, statEntity.getScore());
            st.execute();
        } catch (SQLException e) {
            log.error("Can't insert {}", statEntity, e);
        }
    }
}
