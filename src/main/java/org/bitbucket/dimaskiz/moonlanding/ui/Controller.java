package org.bitbucket.dimaskiz.moonlanding.ui;

import static org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao.MAX_STATS_COUNT;
import static org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao.NAME_MAX_LENGTH;
import static org.bitbucket.dimaskiz.moonlanding.ui.Resources.EXPLOSION_IMAGE;
import static org.bitbucket.dimaskiz.moonlanding.ui.Resources.FLAME_IMAGE;
import static org.bitbucket.dimaskiz.moonlanding.ui.Resources.SHIP_IMAGE;

import java.util.List;
import java.util.Optional;

import org.bitbucket.dimaskiz.moonlanding.config.Config;
import org.bitbucket.dimaskiz.moonlanding.core.GameEngine;
import org.bitbucket.dimaskiz.moonlanding.core.ScoreCalculator;
import org.bitbucket.dimaskiz.moonlanding.core.Telemetry;
import org.bitbucket.dimaskiz.moonlanding.persistence.StatEntity;
import org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Controller {
    @FXML
    private Button btnStart;
    @FXML
    public Button btnStop;
    @FXML
    public Canvas canvas;
    @FXML
    public Slider slider;
    @FXML
    public Label fuelConsumption;

    private GameEngine gameEngine;
    private Config config;
    private StatsDao statsDao;

    public void initialize() {
        log.info("Initializing...");
        slider.setMin(0.0);
        slider.setValue(0.0);
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            gameEngine.setFuelConsumption(newValue.doubleValue());
            fuelConsumption.textProperty().setValue(String.format("%.2f", newValue.doubleValue()));
        });
        slider.setDisable(true);
        btnStop.setDisable(true);
        fuelConsumption.setDisable(true);
        drawIntro();
        log.info("Initialized...");
    }

    private void drawIntro() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setFill(Color.BLACK);
        double x = 5;
        double y = canvas.getHeight() * 0.1;
        gc.fillText("Moon Landing Game\n", 30, y);
        gc.fillText("You need to land a rocket", x, y += 42);
        gc.fillText("Maximum landing speed is 5 m/s", x, y += 15);
        gc.fillText("Use slider to control the acceleration ->", x, y += 15);
        gc.fillText("And remember, the fuel is limited!", x, y += 15);
        gc.fillText("When ready - hit 'Start'", 30, y += 30);
    }

    private void drawStats() {
        List<StatEntity> stats = statsDao.list();
        if (stats.isEmpty()) {
            return;
        }
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        double y = canvas.getHeight() * 0.1 + 70;
        gc.fillText("Stats\n", (canvas.getWidth() - 50) / 2, y);
        double x = (canvas.getWidth() - 50) / 2 - 30;
        y += 22;
        for (StatEntity statEntity : stats) {
            gc.fillText(statEntity.getName(), x, y += 15);
            gc.fillText(String.format("%5d", statEntity.getScore()), x + 100, y);
        }
    }

    private void drawFuelGauge(GraphicsContext gc, double fill, double x, double y) {
        gc.setFill(Color.color(1.0 * (1 - fill), 1.0 * fill, 0.0));
        gc.setStroke(Color.BLACK);
        gc.strokeRect(x - 10, y + 10, 5, 31);
        gc.fillRect(x - 9, y + 11 + (29 * (1 - fill)), 3, 29 * fill);
    }

    public void onBtnStopClick() {
        log.debug("UI: stopGame");
        gameEngine.stopGame();
        btnStart.setDisable(false);
        btnStop.setDisable(true);
        slider.setDisable(true);
        fuelConsumption.setDisable(true);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.fillText("Stopped!", (canvas.getWidth() - 50) / 2, 50);
    }

    public void onBtnStartClick() {
        log.debug("UI: startGame");
        log.info("Started with {}", config);
        slider.setValue(0.0);
        slider.setDisable(false);
        fuelConsumption.setDisable(false);
        btnStop.setDisable(false);
        btnStart.setDisable(true);
        gameEngine = new GameEngine(config);
        gameEngine.startGame(telemetry -> Platform.runLater(() -> {
            GraphicsContext gc = canvas.getGraphicsContext2D();
            gc.setFill(Color.WHITE);
            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
            gc.setFill(Color.GRAY);
            gc.fillRect(50, canvas.getHeight() - 18, canvas.getWidth() - 100, 5);
            double height = (1 - telemetry.getAltitude() / config.getInitialAltitude()) * (canvas.getHeight() - 100) + 50;
            gc.setFill(Color.BLACK);
            gc.fillText(String.format("Altitude\n%.2f", telemetry.getAltitude()), canvas.getWidth() - 75, height);
            gc.fillText(String.format("Speed\n%.2f", telemetry.getSpeed()), 25, height);
            if (telemetry.getStatus() == Telemetry.Status.LANDING) {
                drawFuelGauge(gc, telemetry.getFuel() / config.getInitialFuel(), (canvas.getWidth() - 32) / 2, height - 10);
                gc.drawImage(SHIP_IMAGE, (canvas.getWidth() - 32) / 2, height, 32, 32);
                double flameSize = telemetry.getFuelConsumption() / (config.getBalanceConsumption() * 2);
                double imageSize = 32 * flameSize;
                gc.drawImage(FLAME_IMAGE, (canvas.getWidth() - imageSize) / 2, height + 31, imageSize, imageSize);
            } else if (telemetry.getStatus() == Telemetry.Status.LANDED) {
                drawFuelGauge(gc, telemetry.getFuel() / config.getInitialFuel(), (canvas.getWidth() - 32) / 2, canvas.getHeight() - 60);
                gc.drawImage(SHIP_IMAGE, (canvas.getWidth() - 32) / 2, canvas.getHeight() - 50, 32, 32);
                gc.setFill(Color.BLACK);
                long score = ScoreCalculator.calcScore(config, telemetry);
                gc.fillText("Landed!", (canvas.getWidth() - 50) / 2, 50);
                gc.fillText(String.format("in %.3f seconds", telemetry.getRunTime() / 1000.), (canvas.getWidth() - 50) / 2 - 20, 65);
                gc.fillText(String.format("Your score: %d", score), (canvas.getWidth() - 50) / 2 - 20, 80);
                List<StatEntity> stats = statsDao.list();
                if (stats.isEmpty() || stats.size() < MAX_STATS_COUNT || score >= stats.get(stats.size() - 1).getScore()) {
                    TextInputDialog dialog = new TextInputDialog();
                    dialog.setTitle("New high score!");
                    dialog.setHeaderText("Lucky! You're in a top ten!");
                    dialog.setContentText("Please enter your name:\n(max 10 characters)");
                    Optional<String> result = dialog.showAndWait();
                    result.map(String::trim)
                            .map(name -> name.substring(0, Math.min(name.length(), NAME_MAX_LENGTH)))
                            .ifPresent(name -> statsDao.insert(StatEntity.builder()
                                    .name(name)
                                    .score(score)
                                    .build()));
                }
                drawStats();
            } else if (telemetry.getStatus() == Telemetry.Status.CRUSHED) {
                gc.drawImage(EXPLOSION_IMAGE, (canvas.getWidth() - 32) / 2, canvas.getHeight() - 50, 32, 32);
                gc.setFill(Color.BLACK);
                gc.fillText("Crushed!", (canvas.getWidth() - 50) / 2, 50);
                gc.fillText(String.format("in %.3f seconds", telemetry.getRunTime() / 1000.), (canvas.getWidth() - 50) / 2 - 20, 65);
                drawStats();
            }
        }), () -> Platform.runLater(() -> {
            btnStart.setDisable(false);
            btnStop.setDisable(true);
            slider.setDisable(true);
            fuelConsumption.setDisable(true);
        }));
    }

    public void terminate() {
        if (gameEngine != null) {
            gameEngine.stopGame();
        }
    }

    public void setConfig(Config config) {
        this.config = config;
        slider.setMax(config.getBalanceConsumption() * 2);
    }

    public void setStatsDao(StatsDao statsDao) {
        this.statsDao = statsDao;
    }
}
