package org.bitbucket.dimaskiz.moonlanding.ui;

import java.io.InputStream;
import java.net.URL;

import javafx.scene.image.Image;

public class Resources {

    public static URL MAIN_LAYOUT;
    public static URL MAIN_CSS;
    public static Image SHIP_IMAGE;
    public static Image EXPLOSION_IMAGE;
    public static Image FLAME_IMAGE;

    public static void init() {
        MAIN_LAYOUT = Resources.class.getClassLoader().getResource("moonlanding.fxml");
        assert MAIN_LAYOUT != null : "can't find [moonlanding.fxml]";

        MAIN_CSS = Resources.class.getClassLoader().getResource("moonlanding.css");
        assert MAIN_CSS != null : "can't find [moonlanding.css]";

        InputStream shipImageAsStream = Resources.class.getClassLoader().getResourceAsStream("rocket-icon.png");
        assert shipImageAsStream != null : "can't find [rocket-icon.png]";
        SHIP_IMAGE = new Image(shipImageAsStream);

        InputStream explosionImageAsStream = Resources.class.getClassLoader().getResourceAsStream("explosion.png");
        assert explosionImageAsStream != null : "can't find [explosion.png]";
        EXPLOSION_IMAGE = new Image(explosionImageAsStream);

        InputStream flameImageAsStream = Resources.class.getClassLoader().getResourceAsStream("fire.png");
        assert flameImageAsStream != null : "can't find [fire.png]";
        FLAME_IMAGE = new Image(flameImageAsStream);
    }
}
