package org.bitbucket.dimaskiz.moonlanding.config;

import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@ToString
public class Config {

    /**
     * Initial altitude in meters
     */
    private double initialAltitude;
    /**
     * Initial fuel in tank (liters)
     */
    private double initialFuel;
    /**
     * Consumption, which is completely compensate gravity (liters per second)
     */
    private double balanceConsumption;
    /**
     * Initial fuel consumption (liters per second)
     */
    private double initialConsumption;
    /**
     * Moon gravity (meters per second^2)
     */
    private double moonGravity;
    /**
     * Maximum speed for safe landing
     */
    private double maxSpeed;

    private String dbUrl;

    public Config() {
        Properties props = new Properties();

        String fileName = "moonlanding.properties";
        String fullFileName;
        try {
            URL propertiesURI = ClassLoader.getSystemClassLoader().getResource(fileName);
            fullFileName = propertiesURI.getFile();
            InputStream propertiesIS = propertiesURI.openStream();
            props.load(propertiesIS);
        } catch (Exception e) {
            log.error("Can't load config from file {}", fileName, e);
            return;
        }

        try {
            initialAltitude = Double.valueOf(props.getProperty("altitude.initial"));
            initialFuel = Double.valueOf(props.getProperty("fuel.initial"));
            balanceConsumption = Double.valueOf(props.getProperty("consumption.balance"));
            initialConsumption = Double.valueOf(props.getProperty("consumption.initial"));
            moonGravity = Double.valueOf(props.getProperty("moon.gravity"));
            maxSpeed = Double.valueOf(props.getProperty("speed.max"));
            dbUrl = props.getProperty("db.url");
        } catch (NumberFormatException e) {
            log.error("Can't parse config from file {}", fullFileName, e);
            System.exit(1);
        }

        log.info("Config loaded from file {}", fullFileName);
    }
}
