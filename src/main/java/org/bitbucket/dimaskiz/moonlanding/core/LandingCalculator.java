package org.bitbucket.dimaskiz.moonlanding.core;

import org.bitbucket.dimaskiz.moonlanding.config.Config;

public class LandingCalculator {

    public static void nextStep(Config config, Telemetry telemetry, long timeMillis) {
        double timeInSeconds = timeMillis / 1000.0;
        double deltaSpeedDown = timeInSeconds * config.getMoonGravity();
        double deltaSpeedUp = 0.0;
        double deltaFuel = 0.0;

        if (telemetry.getFuel() > 0.0) {
            deltaSpeedUp = deltaSpeedDown * (telemetry.getFuelConsumption() / config.getBalanceConsumption());
            deltaFuel = telemetry.getFuelConsumption() * timeInSeconds;
        } else {
            telemetry.setFuelConsumption(0.0);
        }

        double currentSpeed = telemetry.getSpeed() + deltaSpeedDown - deltaSpeedUp;
        double currentFuel = telemetry.getFuel() - deltaFuel;
        double currentHeight = telemetry.getAltitude() - currentSpeed * timeInSeconds;

        telemetry.setAltitude(Math.max(currentHeight, 0));
        telemetry.setSpeed(currentSpeed);
        telemetry.setFuel(Math.max(currentFuel, 0));
        telemetry.setRunTime(telemetry.getRunTime() + timeMillis);
        Telemetry.Status status = telemetry.getAltitude() > 0 ?
                Telemetry.Status.LANDING :
                currentSpeed > config.getMaxSpeed() ?
                        Telemetry.Status.CRUSHED :
                        Telemetry.Status.LANDED;
        telemetry.setStatus(status);
    }
}
