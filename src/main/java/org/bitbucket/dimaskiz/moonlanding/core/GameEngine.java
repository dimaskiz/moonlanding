package org.bitbucket.dimaskiz.moonlanding.core;

import java.util.function.Consumer;

import org.bitbucket.dimaskiz.moonlanding.config.Config;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameEngine {

    private final Config config;

    private Thread mainThread;
    private volatile boolean isRunning = false;
    private Consumer<Telemetry> telemetryCallback;
    private Runnable stopCallback;
    private Telemetry telemetry;

    public GameEngine(Config config) {
        this.config = config;
    }

    public void startGame(Consumer<Telemetry> callback, Runnable stopCallback) {
        if (mainThread != null) {
            return;
        }
        this.telemetryCallback = callback;
        this.stopCallback = stopCallback;
        isRunning = true;
        telemetry = new Telemetry(config);
        mainThread = new Thread(new Landing(), "landing");
        mainThread.start();
    }

    public void stopGame() {
        if (mainThread == null) {
            return;
        }
        try {
            isRunning = false;
            mainThread.join();
            mainThread = null;
        } catch (InterruptedException e) {
            log.error("Exception while waiting for game-engine thread to finish", e);
        }
    }

    public void setFuelConsumption(double fuelConsumption) {
        if (mainThread == null || telemetry == null) {
            return;
        }
        telemetry.setFuelConsumption(fuelConsumption);
    }

    public boolean isRunning() {
        return isRunning;
    }

    class Landing implements Runnable {
        private long time = System.currentTimeMillis();

        @Override
        public void run() {
            do {
                long currentTime = System.currentTimeMillis();
                LandingCalculator.nextStep(config, telemetry, currentTime - time);
                telemetryCallback.accept(telemetry);
                log.trace("{}", telemetry);
                time = currentTime;
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    log.error("Interrupted!");

                }
            } while (isRunning && !telemetry.isStopped());
            log.info("{} - {}", telemetry.getStatus(), telemetry);
            isRunning = false;
            stopCallback.run();
        }
    }

}
