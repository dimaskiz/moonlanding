package org.bitbucket.dimaskiz.moonlanding.core;

import org.bitbucket.dimaskiz.moonlanding.config.Config;

public class ScoreCalculator {

    public static long calcScore(Config config, Telemetry telemetry) {
        long fuelScore = fuelScore(config, telemetry);
        long timeScore = timeScore(config, telemetry);
        long speedScore = speedScore(config, telemetry);

        return fuelScore + timeScore + speedScore;
    }

    /**
     * more fuel -> higher score (we love fuel economy)
     * in range 100 - 400
     */
    static long fuelScore(Config config, Telemetry telemetry) {
        return Math.round(100 + 300 * (telemetry.getFuel() / config.getInitialFuel()));
    }

    /**
     * less time -> higher score (we don't like to wait)
     * in range 0 - 300
     */
    static long timeScore(Config config, Telemetry telemetry) {
        double freeFallTimeSeconds = Math.sqrt(2 * config.getInitialAltitude() / config.getMoonGravity());
        double fallingTimeSeconds = telemetry.getRunTime() / 1000.0;
        if (fallingTimeSeconds == 0) {
            return 300;
        }

        return 300 * Math.round(freeFallTimeSeconds / fallingTimeSeconds);
    }

    /**
     * higher speed -> lower score (we prefer comfort landing)
     * in range 0 - 300
     */
    static long speedScore(Config config, Telemetry telemetry) {
        return Math.round(300 * (1 - telemetry.getSpeed() / config.getMaxSpeed()));
    }
}
