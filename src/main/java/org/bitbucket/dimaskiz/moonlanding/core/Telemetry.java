package org.bitbucket.dimaskiz.moonlanding.core;

import static org.bitbucket.dimaskiz.moonlanding.core.Telemetry.Status.CRUSHED;
import static org.bitbucket.dimaskiz.moonlanding.core.Telemetry.Status.LANDED;
import static org.bitbucket.dimaskiz.moonlanding.core.Telemetry.Status.LANDING;

import org.bitbucket.dimaskiz.moonlanding.config.Config;

import lombok.Data;

@Data
public class Telemetry {

    public enum Status {
        LANDING,
        LANDED,
        CRUSHED;
    }

    private Status status = LANDING;
    /**
     * time passed from falling started, milliseconds
     */
    private long runTime;
    /**
     * Current altitude in meters
     */
    private double altitude;
    /**
     * Current speed in meters per second
     */
    private double speed;
    /**
     * fuel left in tank (liters)
     */
    private double fuel;
    /**
     * current fuel consumption in liters per second
     */
    private volatile double fuelConsumption;

    public Telemetry(Config config) {
        altitude = config.getInitialAltitude();
        fuel = config.getInitialFuel();
        fuelConsumption = config.getInitialConsumption();
    }

    public boolean isStopped() {
        return status == LANDED || status == CRUSHED;
    }
}
