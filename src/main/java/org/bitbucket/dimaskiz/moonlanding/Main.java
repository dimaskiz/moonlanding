package org.bitbucket.dimaskiz.moonlanding;

import static org.bitbucket.dimaskiz.moonlanding.ui.Resources.MAIN_LAYOUT;

import java.io.IOException;
import java.sql.Connection;

import org.bitbucket.dimaskiz.moonlanding.config.Config;
import org.bitbucket.dimaskiz.moonlanding.persistence.H2DBConnectionHandler;
import org.bitbucket.dimaskiz.moonlanding.persistence.StatsDao;
import org.bitbucket.dimaskiz.moonlanding.ui.Controller;
import org.bitbucket.dimaskiz.moonlanding.ui.Resources;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main extends Application {

    public static void main(String[] args) {
        Resources.init();
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Thread.currentThread().setName("ui-thread");
        log.info("Program starting...");
        Config config = new Config();

        FXMLLoader fxmlLoader = new FXMLLoader(MAIN_LAYOUT);
        VBox root = fxmlLoader.load();
        Controller controller = fxmlLoader.getController();
        controller.setConfig(config);

        primaryStage.setTitle("Moon Landing Game");
        Scene scene = new Scene(root, 300, 600, Color.BLACK);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        primaryStage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });

        Thread hook = new Thread(() -> {
            log.info("Stopping...");
            controller.terminate();
        });
        hook.setName("shutdown-hook");
        Runtime.getRuntime().addShutdownHook(hook);

        Connection connection = new H2DBConnectionHandler(config.getDbUrl()).getConnection();
        StatsDao statsDao = new StatsDao(connection);
        statsDao.init();
        controller.setStatsDao(statsDao);

        log.info("Program started");
    }
}
