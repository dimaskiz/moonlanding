### Moon Landing Game 
This little project is a primitive JavaFx-based game, inspired by the task from technical interview for Java developer position from this [post](https://www.facebook.com/anna.samodurova/posts/2491661030845788?utm_source=telegram.me&utm_medium=social&utm_campaign=mdtrue*java-developer*v-*x5-retail-gr) by Anna Skripchenko (in Russian), requirements described [here](https://docs.google.com/document/d/1ej4_k1uaXDfMZxNPJuzsH3rZzxFZTU4_7So8fEaN3Y4/edit) (in Russian)

Briefly, in this game you have to land a spaceship, which is have a limited fuel, and the only thing you need to control - is a ship acceleration.

Ship can be landed successfully only with speed not more than 5 m/s 

#### Screenshots
![screenshot](https://bitbucket.org/dimaskiz/moonlanding/downloads/moonlanding.png)
#### How to run this game:
* You can simply [download](https://bitbucket.org/dimaskiz/moonlanding/downloads/) ready build and run it
 ```
 $ java -jar moonlanding-1.1.jar
 ```
* or build a .jar file and run it
```
$ git clone https://bitbucket.org/dimaskiz/moonlanding.git
$ cd moonlanding
$ mvn clean install
$ java -jar ./target/moonlanding-1.2-SNAPSHOT.jar
```
If you are getting an error like
```
Error: Could not find or load main class org.bitbucket.dimaskiz.moonlanding.Main
Caused by: java.lang.NoClassDefFoundError: javafx/application/Application
```
It's mean, your java distribution have no JavaFx, It can be fixed by doing
```
$ sudo apt-get install openjfx
```